provider "aws" {
   region = "us-east-1"  
}


locals {
  website_domain = aws_s3_bucket.website.website_domain
}

# S3 bucket with public read
resource "aws_s3_bucket" "website" {
  bucket        = var.www_domain_url
  acl           = "public-read"
  policy        = file("bucket_policy.json")
  force_destroy = true
  website {
    index_document = "index.html"
    error_document = "index.html"

  }
}

# Null resource is used to sync files from your local directory
# to they S3 bucket
resource "null_resource" "remove_and_upload_to_s3" {
  provisioner "local-exec" {
    command = "aws s3 sync ./dist/frank-martinez-resume/ s3://${aws_s3_bucket.website.id}"
  }

  depends_on = [aws_s3_bucket.website]

  # This will force the files to be uploaded everytime your
  # website is build locally.
  triggers = {
    etag = filemd5(abspath("./dist/frank-martinez-resume/index.html"))
  }
}

// Use the AWS ACM to create an SSL cert for the domain.
// There is an email approval component that needs to be accepted
// before the resource is created.
resource "aws_acm_certificate" "cert" {
  // Wild card the cert to use sub domains later
  domain_name       = "*.${var.root_domain_name}"
  validation_method = "EMAIL"

  // The root domain should be valid and we'll be
  // redirecting to www. domain immediatly
  subject_alternative_names = [var.root_domain_name]

}

// route53 record created in a cross account
module "route53" {
  source                 = "./route53"
  root_domain_name       = var.root_domain_name
  www_domain_url         = var.www_domain_url
  cloudfront_domain_name = aws_cloudfront_distribution.www_dist.domain_name
  cloudfront_zone_id     = aws_cloudfront_distribution.www_dist.hosted_zone_id
}

variable "www_domain_url" {
  default = "www.frankmartinez.info"
}

variable "root_domain_name" {
  default = "frankmartinez.info"
}
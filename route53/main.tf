provider "aws" {
  region = "us-east-1"
  assume_role {
    role_arn = "arn:aws:iam::004592438831:role/Route53CrossAccountRole"
  }
}

// Pick the right zone
data "aws_route53_zone" "selected" {
  name = var.root_domain_name
}

// Route53 to point to the CF Dist
resource "aws_route53_record" "www" {
  zone_id = data.aws_route53_zone.selected.zone_id
  name    = var.www_domain_url
  type    = "A"

  alias {
    name                   = var.cloudfront_domain_name
    zone_id                = var.cloudfront_zone_id
    evaluate_target_health = false
  }
}
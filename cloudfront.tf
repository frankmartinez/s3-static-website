resource "aws_cloudfront_distribution" "www_dist" {
  // Origin is where CF gets its content from
  origin {
    custom_origin_config {
      // Default Ports
      http_port              = "80"
      https_port             = "443"
      origin_protocol_policy = "http-only"
      origin_ssl_protocols   = ["TLSv1", "TLSv1.1", "TLSv1.2"]
    }

    // S3 buckets URL
    domain_name = aws_s3_bucket.website.website_endpoint
    // The origin
    origin_id = var.www_domain_url
  }

  enabled             = true
  default_root_object = "index.html"

  // All values are defaults
  default_cache_behavior {
    viewer_protocol_policy = "redirect-to-https"
    compress               = true
    allowed_methods        = ["GET", "HEAD"]
    cached_methods         = ["GET", "HEAD"]
    // This needs to match the orign_id
    target_origin_id = var.www_domain_url
    min_ttl          = 0
    default_ttl      = 86400
    max_ttl          = 3156000

    forwarded_values {
      query_string = false
      cookies {
        forward = "none"
      }
    }
  }

  // Ensuring we hit this distribution using www
  // rather than the domain CF produces
  aliases = [var.www_domain_url]

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

  // Load the certificate
  viewer_certificate {
    acm_certificate_arn = aws_acm_certificate.cert.arn
    ssl_support_method  = "sni-only"
  }
}
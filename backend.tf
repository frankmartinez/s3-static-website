# Configure the backend.
# Let's save the Terraform Stage in Terrafom Cloud
terraform {
  backend "remote" {
    hostname     = "app.terraform.io"
    organization = "frank-martinez"

    workspaces {
      name = "frank-martinez-resume"
    }
  }
}